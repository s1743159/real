package uk.ac.ed.pguaglia.real.lang;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.DatabaseException;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.Table;

public class BaseExpression extends Expression {

	private final String name;

	public BaseExpression( String name ) {
		super(Expression.Type.BASE);
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public String toSyntaxTreeString(String prefix, Schema schema) {
		if (schema != null && this.isView(schema)) {
			return schema.getViewDefinition(name).toSyntaxTreeString(prefix, schema);
		}
		return prefix + this.toString();
	}

	@Override
	public Set<String> signature(Schema schema) throws SchemaException {
		List<String> attr = schema.getTableAttributes(name);
		if (attr == null) {
			Expression def = schema.getViewDefinition(name);
			if (def == null) {
				throw SchemaException.tableNotFound(this, schema);
			} else {
				return def.signature(schema);
			}
		}
		return new HashSet<>(attr);
	}

	@Override
	public Table execute(Database db, boolean bags) throws DatabaseException {
		Schema sch = db.schema();
		signature(sch);
		if (isTable(sch)) {
			Table tbl = db.fetchTable(name);
			return bags ? tbl : tbl.distinct();
		}
		if (isView(sch)){
			return db.getView(name, bags);
		}
		throw new IllegalStateException();
	}

	public boolean isTable(Schema schema) {
		return schema.hasTable(name);
	}

	public boolean isView(Schema schema) {
		return schema.hasView(name);
	}

	@Override
	public Set<String> getBaseNames() {
		Set<String> base = new HashSet<>();
		base.add(name);
		return base;
	}
}
