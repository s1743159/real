package uk.ac.ed.pguaglia.real.lang;

public class Union extends BinaryOperation {

	public Union ( Expression left, Expression right ) {
		super( left, right, Expression.Type.UNION );
	}
}
