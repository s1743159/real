package uk.ac.ed.pguaglia.real.lang;

import java.util.Map;

public class Disjunction extends BinaryCondition {

	public Disjunction ( Condition left, Condition right ) {
		super( left, right, Condition.Type.DISJUNCTION );
	}

	@Override
	public boolean satisfied(String[] record, Map<String, Integer> attr) {
		return super.leftCondition.satisfied(record, attr)
				|| super.rightCondition.satisfied(record, attr);
	}
}
