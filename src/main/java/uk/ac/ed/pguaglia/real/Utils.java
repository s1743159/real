package uk.ac.ed.pguaglia.real;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.lang.BaseExpression;
import uk.ac.ed.pguaglia.real.lang.Expression;

public class Utils {

	public static<T> Set<T> clone(Set<T> input) {
		return new HashSet<T>(input);
	}

	public static<K,V> boolean isInjective(Map<K,V> input) {
		Map<V,K> inv = new HashMap<V,K>();
		for (Map.Entry<K,V> entry : input.entrySet()) {
			if (inv.containsKey(entry.getValue())) {
				return false;
			} else {
				inv.put(entry.getValue(), entry.getKey());
			}
		}
		return true;
	}
	
	public static String toSyntaxTreeString(Expression expr, Schema schema, String prefix, String add) {
		String s;
		if ((expr.getType() == Expression.Type.BASE)
				&& ((schema == null) || ((BaseExpression) expr).isTable(schema))) {
			s = expr.toSyntaxTreeString("", schema);
			s += "\n" + prefix;
		} else {
			s = expr.toSyntaxTreeString(prefix + add, schema);
		}
		return s;
	}
}
