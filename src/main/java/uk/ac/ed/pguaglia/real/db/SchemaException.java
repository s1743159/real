package uk.ac.ed.pguaglia.real.db;

import uk.ac.ed.pguaglia.real.lang.BaseExpression;
import uk.ac.ed.pguaglia.real.lang.Expression;

@SuppressWarnings("serial")
public class SchemaException extends DatabaseException {

	private static final String tableNotFoundMsg =
			"Table name \"%s\" not found";
	private static final String attributeNotFoundMsg =
			"No attribute named \"%s\" in subexpression";
	private static final String noSameAttributesMsg =
			"The subexpressions must have the same set of attributes";
	private static final String noDisjointAttributesMsg =
			"The subexpressions must have disjoint sets of attributes";
	private static final String renamingDropsAttributesMsg =
			"The renaming drops attributes in subexpression";

	private final Schema schema;
	private final Expression expression;

	public SchemaException(String msg, Expression expr, Schema sch) {
		super(msg);
		this.schema = sch;
		this.expression = expr;
	}

	public Expression getExpression() {
		return this.expression;
	}

	public Schema getSchema() {
		return this.schema;
	}

	public static SchemaException tableNotFound(BaseExpression tbl, Schema sch) {
		String msg = String.format(tableNotFoundMsg, tbl);
		return new SchemaException(msg, tbl, sch);
	}

	public static SchemaException attributeNotFound(String attr, Expression expr, Schema sch) {
		String msg = String.format(attributeNotFoundMsg, attr);
		return new SchemaException(msg, expr, sch);
	}

	public static SchemaException noSameAttributes(Expression expr, Schema sch) {
		return new SchemaException(noSameAttributesMsg, expr, sch);
	}

	public static SchemaException noDisjointAttributes(Expression expr, Schema sch) {
		return new SchemaException(noDisjointAttributesMsg, expr, sch);
	}

	public static SchemaException renamingDropsAttributes(Expression expr, Schema sch) {
		return new SchemaException(renamingDropsAttributesMsg, expr, sch);
	}
}
